﻿$ErrorActionPreference = 'Stop'; # stop on all errors
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url64      = 'https://packages.chef.io/files/stable/chef-workstation/0.1.120/windows/2016/chef-workstation-0.1.120-1-x64.msi'

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'msi'
  url64bit      = $url64

  softwareName  = 'chef-workstation' 

  checksum64      = '7E5F6F592D9DCF683D01DBADE9E4CF3F614C254EB9CEBB35149342A4BFD7CB5D'
  checksumType64  = 'sha256' 

  # MSI
  silentArgs    = "/qn /quiet /norestart"
  validExitCodes= @(0, 3010, 1641)
}

Install-ChocolateyPackage @packageArgs
